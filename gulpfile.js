var gulp = require('gulp'),
    gutil = require('gulp-util'),
    livereload = require('gulp-livereload'),
    watch = require('gulp-watch');
    wait = require('gulp-wait');

// js file 
var concat = require('gulp-concat'),
    stripDebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify');

// angularjs
var templateCache = require('gulp-angular-templatecache');
var minifyHtml = require("gulp-minify-html");
var gettext = require('gulp-angular-gettext');
var ngmin = require('gulp-ngmin');

// less->css
var less = require('gulp-less'),
    autoprefix = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css');

// html
var minifyHtml = require('gulp-minify-html');
// images
var imagemin = require('gulp-imagemin');

var paths = {
  js: './public/app/**/*.js',
  img: './public/img/**/*',
  less: './public/css/**/*.less',
  html: './public/app/**/*.html'
};

gulp.task('less', function() {
    // console.log('-----build main.less');
  gulp.src('./public/css/main.less')
    .pipe(less())
    .pipe(autoprefix('last 2 versions'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./public/css'))
    .pipe(livereload());
});

gulp.task('less-watch',function() {
    // console.log('----watch');
  watch({glob: paths.less}, ['less'] )
});

// another style,启动的时候比第一个slow
gulp.task('less-watch-1',function() {
    gulp.watch(paths.less, ['less']);
});


gulp.task('js', function() {
  gulp.src(paths.js)
    .pipe(ngmin())
    .pipe(concat('app.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('./public/build/'))
    .pipe(livereload());
});

//  ============ template cache =====
gulp.task('cache', function () {
    gulp.src(paths.html)
        .pipe(minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(templateCache() )
        .pipe(gulp.dest('./public/build/'));
});

gulp.task('html', function() {
  gulp.src(paths.html)
    .pipe(minifyHtml())
    .pipe(gulp.dest('./public/build/html/'));
});

gulp.task('img', function() {
  gulp.src('./public/img/')
    .pipe(imagemin())
    .pipe(gulp.dest('./public/build/img/'));
});

// use chrome plugin :https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei
gulp.task('codekit',function(){
	gulp.src([paths.less, paths.html, paths.js ])
    .pipe(watch())
    // coddkit is slow at compile less, so i have to wait for that shit.......
    .pipe(wait(1500))
    .pipe(livereload());
})
gulp.task('only-reload',function(){
   livereload.changed();
})

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(paths.html).on('change', livereload.changed);
    gulp.watch(paths.js).on('change', livereload.changed);
    gulp.watch(paths.img, ['img']);
     gulp.watch(paths.less, ['less']);
});


gulp.task('default', ['watch']);
gulp.task('build', ['cache','less','js','img']);


// if i want to watch file changes:
gulp.task("watch-build", ["imagemin", "htmlpage", "scripts", "styles"], function() {
  gulp.watch('./public/app/**/*.html', ["cache"]);
  gulp.watch('./public/app/**/*.js', ["js"]);
  gulp.watch('./public/css/*.less', ["less"]);
});


