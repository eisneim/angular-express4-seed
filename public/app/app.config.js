﻿// if user visit some link that require authentication ,redirect this user;
app.config(['$httpProvider', function ($httpProvider) {

    var myInterceptor = function($q, $location) { 
        return {
            // response: function(response) { 
            //     // do something on success
            //     if (response.status === 401) {

            //     }else{
            //         return response || $q.when(response);
            //     }
            // },
            responseError: function(rejection) {
                if (rejection.status === 401) {
                    $location.url('/login');
                }else if(rejection.status === 404){
                    $location.url('/notFound');
                }
                return $q.reject(rejection);
            }
        }
    }

    $httpProvider.interceptors.push(myInterceptor); 

    $httpProvider.cache = true;

}]);
