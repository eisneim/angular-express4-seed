﻿var app = angular.module('app', [
    'ngAnimate',        // animations
    'ngRoute',          // routing
    'ngSanitize',       // sanitizes html bindings 

    // 3rd Party Modules
    // 'angularFileUpload', //file upload for images and other stuff
    // 'chieffancypants.loadingBar',//top loading bar
]);

app.run(['$rootScope',
    function ($rootScope) {
    
    console.log('the app is bootstraped!');

}]);
