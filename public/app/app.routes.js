app.config( function($routeProvider,$locationProvider){
	$locationProvider.html5Mode(true);

	$routeProvider
	.when('/',{
			templateUrl:'app/home/home.html',
		}
	)
	.when('/login',{
			template:'login',
		}
	)
	.when('/notFound',{
		template:'<h1> 404 not found</h1>'
	})
	.otherwise({
		template:'this is not exits! 404<a href="#/">back</a>'
	});
	
});

