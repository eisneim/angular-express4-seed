Angular-express4-seed
======================
常规Angular、NodeJS、Express、mongodb应用的初始文件。请根据对前端需要用到的包修改bower.json

#安装：

    npm install
    npm postinstall
    
    // 然后要编译less文件 
    gulp less

如果你们需要做前端的设计，写less代码，可以开启编译+浏览器自动刷新功能，你们可以安装codekit或者prepros，或者直接使用我设置好的gulp配置，每当less文件改变时，自动刷新浏览器(需要安装chrome livereload插件),改变js，HTML文件时也会刷新浏览器。

    gulp watch


# 一些可以学习的教程 
 - [csrf 安全问题](http://www.mircozeiss.com/using-csrf-with-express-and-angular/)
 - [socket.io 认证问题](http://howtonode.org/socket-io-auth)