var mongoose = require('../config/config.db');
var Schema= mongoose.Schema;

var UserSchema = new Schema({
		username:{ type: String, trim: true ,unique:true},//if there is a space,replace it with _
		//user name it will be used in url
		email:{ type: String,unique:true, required: '{PATH} 是必填项!' },
		// password: String,
		salt: String,
    	hash: String,
		
		avartar:{type:String},//,default:'img/avartar.jpg'
		
		// not important: 
		description:String,
		city:String,
		phone:{ type: Number, max:[13, ' `{PATH}`项的值: ({VALUE}) 超过了最大值 ({MAX}).']},

	});

	// User.find() -> users[0].greet()
	UserSchema.methods.greet = function() {
	  return 'Hello, ' + this.username;
	};

module.exports = mongoose.model('User',UserSchema);