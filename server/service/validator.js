function escHtml(text) {
  return text
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
}
module.exports = function(app){
	// var expressValidator = app.validator;
	var validator= {};
	// var vali = require('validator'),
	// 	sani = vali.sanitize,
	// 	Filter = vali.Filter;

	// 	Filter.prototype.escHtml = function() {
	// 	    this.modify(this.str.replace(/&/g, "&amp;")
	// 	      .replace(/</g, "&lt;")
	// 	      .replace(/>/g, "&gt;")
	// 	      .replace(/"/g, "&quot;")
	// 	      .replace(/'/g, "&#039;"));
	// 	    return this.str;
	// 	}


	validator.signup=function(req){
		req.body.username = escHtml(req.body.username);
		req.sanitize('username').trim();
		req.sanitize('email').trim();
		req.sanitize('city').trim();
		req.assert('username', 'name should between 2-15 chars, and not contain special chars')
			.len(2,15).regex(/^[a-zA-Z0-9_-\s]{2,15}$/).notEmpty();
		req.assert('city', 'City should between 2-20 chars').len(2,20).notEmpty();
		req.assert('password', 'Password must between 6-20 chars,not empty').len(6,20).notEmpty();
		req.assert('passwordConfirm', 'Two passwords are not same').equals(req.body.password).notEmpty();
		req.assert('country', 'Country is required and between 2-20 chars').len(2,20).notEmpty();
	    return req.validationErrors();
	}

	validator.updesign = function(req){
		req.body.title = escHtml(req.body.title);
		req.body.description = escHtml(req.body.description);
		req.sanitize('title').trim();
		req.sanitize('description').trim();
		req.assert('title', 'title should between 3-70 chars, and not contain special chars')
			.len(3,70).regex(/^[^\<\>\\\/\`\~\^\;]{3,70}$/).notEmpty();
		req.assert('style', 'style should between 3-50 chars, and not contain special chars')
			.len(3,30).regex(/^[^\<\>\\]{3,30}$/).notEmpty();
		req.assert('category', 'style should between 3-50 chars, and not contain special chars')
			.len(3,30).regex(/^[^\<\>\\\/]{3,30}$/).notEmpty();
		req.assert('description', 'description should between 3-500 chars').regex(/^[^\<\>\\\/]{3,500}$/)
			.len(3,500).notEmpty();
		req.assert('img', 'img should not be empty').notEmpty();
		req.assert('tags', 'tags should not be empty').notEmpty();
		req.assert('uptype', 'upload type only in "cloth" or "design" should not be empty')
			.regex(/(design|cloth)/).notEmpty();

		return req.validationErrors();
	}
	
	validator.profile = {};
	validator.profile.description = function(req){
		// req.body.description = escHtml(req.body.description);
		req.sanitize('description').trim();
		req.assert('description', 'Description should between 5-500 chars, and not contain special chars')
			.len(5,500).notEmpty();
		return req.validationErrors();
	}
	validator.profile.site = function(req){
		req.body.sitename = escHtml(req.body.sitename);
		req.sanitize('sitelink').trim();
		req.sanitize('sitename').trim();
		req.assert('sitename', 'Sitename should between 3-70 chars, and not contain special chars')
			.len(3,70).regex(/^[^\<\>\\\/\`\~\^\;]{3,70}$/).notEmpty();
		return req.validationErrors();
	}
	validator.detail = function(req,type){
		switch(type){
			case 'description':
				req.sanitize('description').trim();
				req.assert('description', 'Description should between 5-500 chars, and not contain special chars')
				.len(5,500).notEmpty();
			break;
			case 'title':
				req.body.title = escHtml(req.body.title);
				req.sanitize('title').trim();
				req.assert('title', 'title should between 3-50 chars, and not contain special chars')
				.regex(/^[^\<\>\\\/\`\~\^\;]{3,50}$/).notEmpty();
			break;
			case 'newtag':
				req.body.newtag = escHtml(req.body.newtag);
				req.sanitize('newtag').trim();
				req.assert('newtag', 'tag should between 2-15 chars, and not contain special chars')
				.regex(/^[^\<\>\\\/\`\~\^\;]{2,15}$/).notEmpty();
			break;
			case 'category':
				req.body.category = escHtml(req.body.category);
				req.sanitize('category').trim();
				req.assert('category', 'category should between 2-15 chars, and not contain special chars')
				.regex(/^[^\<\>\\\/\`\~\^\;]{2,15}$/).notEmpty();
			break;
			case 'style':
				req.body.style = escHtml(req.body.style);
				req.sanitize('style').trim();
				req.assert('style', 'style should between 2-15 chars, and not contain special chars')
				.regex(/^[^\<\>\\\/\`\~\^\;]{2,15}$/).notEmpty();
			break;
			// default:

			// break;

		}
		
		return req.validationErrors();
	}

	return validator;
}

// function login_form_validate(req){
// 	req.assert('email', '请输入正确的email地址').isEmail().notEmpty();
// 	req.assert('password', '密码必填且必须在4至12字之间').len(4,12).notEmpty();
// 	return req.validationErrors();
// }
// function signup_form_validate(req){
// 	req.sanitize('username').trim();
// 	req.assert('username', '用户名必须在2至10个字符之间').len(2,10).notEmpty();
// 	req.assert('email', '请输入正确的email地址').isEmail().notEmpty();
// 	req.assert('password', '密码必填且必须在4至12字之间').len(4,12).notEmpty();
// 	req.assert('password_confirm', '两次密码不一致！').equals(req.body.password).notEmpty();
// 	req.assert('phone', '请输入合法的电话号码!').isNumeric().len(4,15).notEmpty();
// 	return req.validationErrors();
// }