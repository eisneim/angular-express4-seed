var path = require('path');
var rootPath = path.normalize(__dirname + '/../../');

module.exports = function(env){
  var config = {
    rootPath: rootPath,
    env: env,
    cookie_secret:'-----dfdf'
  }

  config.db = env=='development'?
      'mongodb://localhost/one_mile_away':
      'mongodb://';
  // those are for connect-mongo
  config.dbPort = env=='development'? 27017:27017;
  config.dbHost = env=='development'? 'localhost':'localhost';

  config.port =  process.env.PORT || ( env=='development'? 3000 : 5001 )

  return config

}