var express = require('express');
var app = express();

var fs = require('fs');

var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var config = require('./server/config/config')(env);

require('./server/config/config.express')(app, config);

// all routes
fs.readdirSync(config.rootPath + 'server/routes').forEach(function(name){
	if(name.substr(-3) =='.js'){
		require('./server/routes/'+name)(app);
	}
});

//  for angularjs route html5 mode
app.all('/*',function(req, res){
		var user = req.user ? 
    {
      	_id: req.user.user._id,
       	email: req.user.user.email,
       	username: req.user.user.username,
    }
    : null;

    res.render('index',{
      bootstrapUser:user,
      title:'express-anularjs site'
    });
});

app.listen(config.port);
console.log('Listening on port ' + config.port + '...');